<?php

// @todo: reaname preprocessor -> validator

$plugin = array(
  'element_type' => 'textarea',
  'preprocessor' => 'feaval_preprocessor__input__textarea',
  'response' => 'feaval_validator__input__textarea_response',
  'scope_callback' => 'feaval_preprocessor__input__textarea_scope',
);

function feaval_preprocessor__input__textarea(&$element, &$form_state) {
  $element['#ajax'] = feaval_ajax_defaults($form_state);
  $element['#ajax'] += array(
    'event' => 'focusout',
  );
}

// @todo: The contents of the function is similar to many other plugins.
// Move into separate function.
function feaval_preprocessor__input__textarea_selector($form, $form_state, $triggering_element) {
  // see drupal_html_id()
  $id = $triggering_element['#id'];
  $parts = explode('--', $id);
  $id = $parts[0];
  if (!empty($parts[1]) && $parts[1] > 2) {
    $id .= '--' . ($parts[1]-1);
  }
  $selector = "#{$id}";

  return $selector;
}


// @todo
function feaval_validator__input__textarea_response($form, $form_state, $triggering_element) {
  $selector = feaval_preprocessor__input__textarea_selector($form, $form_state, $triggering_element);
  // @todo: Include error text here.
  $response = array(
    'selector' => $selector,
  );
  return $response;
}


// @todo: The contents of the function is similar to many other plugins.
// Move into separate function.
function feaval_preprocessor__input__textarea_scope($form, $form_state, $triggering_element) {

  $scope = array();

  $array_parents = $triggering_element['#array_parents'];
  while (!empty($array_parents)) {
    $scope = drupal_array_get_nested_value($form, $array_parents);
    if (!empty($scope['#feaval']['#plugin'])) {
      break;
    }
    array_pop($array_parents);
  }

  // @todo: Set error if scope is not found.

  return $scope;
}
