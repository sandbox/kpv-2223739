<?php

$plugin = array(
  'element_type' => 'textfield',
  'preprocessor' => 'feaval_preprocessor__input__textfield',
  'response' => 'feaval_validator__input__textfield_response',
  'scope_callback' => 'feaval_validator__input__textfield_scope',
);

function feaval_preprocessor__input__textfield(&$element, &$form_state) {
  $element['#ajax'] = feaval_ajax_defaults($form_state);
  $element['#ajax'] += array(
    'event' => 'focusout',
  );
}

// @todo: The contents of the function is similar to many other plugins.
// Move into separate function.
function feaval_validator__input__textfield_selector($form, $form_state, $triggering_element) {
  // see drupal_html_id()
  $id = $triggering_element['#id'];
  $parts = explode('--', $id);
  $id = $parts[0];
  if (!empty($parts[1]) && $parts[1] > 2) {
    $id .= '--' . ($parts[1]-1);
  }
  $selector = "#{$id}";

  return $selector;
}

// @todo
function feaval_validator__input__textfield_response($form, $form_state, $triggering_element) {
  $selector = feaval_validator__input__textfield_selector($form, $form_state, $triggering_element);
  // @todo: Include error text here.
  $response = array(
    'selector' => $selector,
  );
  return $response;
}

// @todo: The contents of the function is similar to many other plugins.
// Move into separate function.
function feaval_validator__input__textfield_scope($form, $form_state, $triggering_element) {

  $scope = array();

  $array_parents = $triggering_element['#array_parents'];
  while (!empty($array_parents)) {
    $scope = drupal_array_get_nested_value($form, $array_parents);
    if (!empty($scope['#feaval']['#plugin'])) {
      break;
    }
    array_pop($array_parents);
  }

  // @todo: Set error if scope is not found.

  return $scope;
}
