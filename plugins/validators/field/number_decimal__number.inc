<?php

$plugin = array(
  'field_type' => 'number_decimal',
  'widget_type' => 'number',
  'preprocessor' => 'feaval_preprocessor__number_decimal__number',
  'response' => 'feaval_preprocessor__number_decimal__number__response',
  'scope_callback' => 'feaval_preprocessor__number_decimal__number__scope',
);

function feaval_preprocessor__number_decimal__number(&$element, &$form_state, $context) {
  $element['value']['#ajax'] = feaval_ajax_defaults($form_state);
  $element['value']['#ajax'] += array(
    'event' => 'focusout',
  );
}

function feaval_preprocessor__number_decimal__number__selector($form, $form_state, $triggering_element) {
  // see drupal_html_id()
  $id = $triggering_element['#id'];
  $parts = explode('--', $id);
  $id = $parts[0];
  if (!empty($parts[1]) && $parts[1] > 2) {
    $id .= '--' . ($parts[1]-1);
  }
  $selector = "#{$id}";

  return $selector;
}

// @todo
function feaval_preprocessor__number_decimal__number__response($form, $form_state, $triggering_element) {
  $selector = feaval_preprocessor__number_decimal__number__selector($form, $form_state, $triggering_element);
  // @todo: Include error text here.
  $response = array(
    'selector' => $selector,
  );
  return $response;
}

function feaval_preprocessor__number_decimal__number__scope($form, $form_state, $triggering_element) {

  $scope = array();

  $array_parents = $triggering_element['#array_parents'];
  while (!empty($array_parents)) {
    $scope = drupal_array_get_nested_value($form, $array_parents);
    if (!empty($scope['#feaval']['#plugin'])) {
      break;
    }
    array_pop($array_parents);
  }

  // @todo: Set error if scope is not found.

  return $scope;
}
