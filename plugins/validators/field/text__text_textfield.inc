<?php

$plugin = array(
  'field_type' => 'text',
  'widget_type' => 'text_textfield',
  'preprocessor' => 'feaval_preprocessor__text__text_textfield',
  'response' => 'feaval_preprocessor__text__text_textfield__response',
  'scope_callback' => 'feaval_preprocessor__text__text_textfield__scope',
);

function feaval_preprocessor__text__text_textfield(&$element, &$form_state, $context) {
  $element['value']['#ajax'] = feaval_ajax_defaults($form_state);
  $element['value']['#ajax'] += array(
    'event' => 'focusout',
  );
}

function feaval_preprocessor__text__text_textfield__selector($form, $form_state, $triggering_element) {
  // see drupal_html_id()
  $id = $triggering_element['#id'];
  $parts = explode('--', $id);
  $id = $parts[0];
  if (!empty($parts[1]) && $parts[1] > 2) {
    $id .= '--' . ($parts[1]-1);
  }
  $selector = "#{$id}";

  return $selector;
}


// @todo
function feaval_preprocessor__text__text_textfield__response($form, $form_state, $triggering_element) {
  $selector = feaval_preprocessor__text__text_textfield__selector($form, $form_state, $triggering_element);
  // @todo: Include error text here.
  $response = array(
    'selector' => $selector,
  );
  return $response;
}


function feaval_preprocessor__text__text_textfield__scope($form, $form_state, $triggering_element) {

  $scope = array();

  $array_parents = $triggering_element['#array_parents'];
  while (!empty($array_parents)) {
    $scope = drupal_array_get_nested_value($form, $array_parents);
    if (!empty($scope['#feaval']['#plugin'])) {
      break;
    }
    array_pop($array_parents);
  }

  // @todo: Set error if scope is not found.

  return $scope;
}
