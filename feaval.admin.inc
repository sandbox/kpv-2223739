<?php

// @todo: Add content type settings for generic elements.

/**
 * Feaval settings list page callback.
 *
 * @todo
 * Add link to delete certain settings.
 */
function feaval_settings_list() {
  $output = '';
  $generic_rules = feaval_settings_rule_load_multiple('generic');
  $field_rules = feaval_settings_rule_load_multiple('field');
  $output .= theme('feaval_rules_list', array('type' => 'generic', 'items' => $generic_rules));
  $output .= theme('feaval_rules_list', array('type' => 'field', 'items' => $field_rules));
  return $output;
}


/**
 * Feaval generic rule add/edit form
 *
 * @todo
 * Add "Rule name" field.
 */
function feaval_settings_generic_rule_form($form, &$form_state) {
  // set defaults
  $rule_id = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;
  // @todo: Use 'values' instead of 'input'
  if (!empty($form_state['input'])) {
    $name = !empty($form_state['input']['name']) ? $form_state['input']['name'] : '';
    $form_id_pattern = !empty($form_state['input']['form_id_pattern']) ? $form_state['input']['form_id_pattern'] : '';
    $rules = !empty($form_state['input']['rules']) ? $form_state['input']['rules'] : '';
  }
  else {
    if ($rule_id) {
      $rule = feaval_settings_rule_load('generic', $rule_id);
      if (empty($rule)) {
        // Return 'page not found' if rule for rule_id doesn't exist.
        drupal_not_found();
        drupal_exit();
      }
      $name = $rule['name'];
      $form_id_pattern = $rule['form_id_pattern'];
      $rules = $rule['rules'];
    }
    else {
      $name = '';
      $form_id_pattern = '';
      $rules = '';
    }
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $name,
    '#required' => TRUE,
  );
  $form['form_id_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('form_id pattern'),
    '#description' => t('Form $form_id or regex pattern to match (do not include !regex_start and !regex_finish, these will be added automatically).', array('!regex_start' => '"/^"', '!regex_finish' => '"$/"')),
    '#default_value' => $form_id_pattern,
    '#required' => TRUE,
    '#element_validate' => array('_feaval_settings_form_textfield_validate'),
  );
  $form['rules'] = array(
    '#type' => 'textarea',
    '#title' => t('Form elements'),
    '#description' => t('List #array_parents for enabled generic elements, one for each row. Components must be delimited with "|" character. E.g. "first|second|third" for $form["first"]["second"]["third"] form element. Use "*" to match all elements, and "-" to exclude certain elements, e.g. "- first|second|third".'),
    '#default_value' => $rules,
    '#required' => TRUE,
    '#element_validate' => array('_feaval_settings_form_textarea_validate'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}


/**
 * Validate settings forms' textfields.
 *
 * @todo
 */
function _feaval_settings_form_textfield_validate($element, &$form_state, $form) {
  $values = $form_state['values'];
  switch ($element['#name']) {
  case 'form_id_pattern' :
    $value = $values['form_id_pattern'];
    // First check if the value is a valid pattern.
    // http://stackoverflow.com/questions/8825025/test-if-a-regular-expression-is-a-valid-one-in-php
    $value_pattern = '/^' . $value . '$/';
    if (@preg_match($value_pattern, 'subject test string') === false) {
      // the regex failed and is likely invalid
      form_error($element, t('Invalid regex sytax'));
    }
    /*
    // And then check pattern itself.
    // @todo: Check pattern
    $pattern = '/^[a-zA-Z0-9_]+$/';
    if (!preg_match($pattern, $value)) {
      // @todo: Check error message text
      form_error($element, t('Wrong pattern'));
    }
    */
    break;
  case 'entity_type' :
    $value = $values['entity_type'];
    $types = array_keys(entity_get_info());
    if (!in_array($value, $types)) {
      form_error($element, t("Entity type doesn't exist."));
    }
    break;
  case 'entity_bundle' :
    $value = $values['entity_bundle'];
    $types = entity_get_info();
    $bundles = array();
    foreach ($types as $type) {
      $bundles += $type['bundles'];
    }
    $bundles = array_keys($bundles);
    if (!in_array($value, $bundles)) {
      form_error($element, t("Bundle type doesn't exist."));
    }
    break;
  }
}

/**
 * Validate settings forms' textareas.
 *
 * @todo
 * Check for empty lines.
 */
function _feaval_settings_form_textarea_validate($element, &$form_state, $form) {
  $values = $form_state['values'];
  switch ($element['#name']) {
  case 'rules' :
    $value = $values['rules'];
    $lines = _feaval_settings_textarea_lines($value);
    foreach ($lines as $k => $line) {
      // @todo: check pattern
      $pattern = '/^-?(\w|\|)+$/';
      if ($line != "*" && !preg_match($pattern, $line)) {
        form_error($element, t("Wrong format on line !num.", array('!num' => $k+1)));
      }
    }
    break;
  case 'fields_list' :
    $value = $values['fields_list'];
    $lines = _feaval_settings_textarea_lines($value);
    foreach ($lines as $k => $line) {
      // @todo: check pattern
      $pattern = '/^-?(\w|\|)+$/';
      if ($line != "*" && !preg_match($pattern, $line)) {
        form_error($element, t("Wrong format at line !num.", array('!num' => $k+1)));
      }
    }
    break;
  }
}


/**
 * Explode textarea value and return array of clean lines.
 */
function _feaval_settings_textarea_lines($value) {
  $lines = array();
  foreach (explode("\n", $value) as $k => $v) {
    $v = trim($v);
    if (empty($v)) {
      continue;
    }
    $lines[] = $v;
  }
  return $lines;
}

/**
 * Feaval generic rule add/edit form validate
 *
 * @todo
 */
function feaval_settings_generic_rule_form_validate($form, &$form_state) {
}


/**
 * Feaval generic rule add/edit form submit
 *
 * @todo
 */
function feaval_settings_generic_rule_form_submit($form, &$form_state) {
  $rule_id = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;

  $rule = array(
    'name' => $form_state['values']['name'],
    'form_id_pattern' => $form_state['values']['form_id_pattern'],
    'rules' => $form_state['values']['rules'],
  );

  // Save current rule.
  feaval_settings_rule_save('generic', $rule, $rule_id);
  drupal_set_message(t('Changes succesfully saved.'));
  $form_state['redirect'] = 'admin/config/system/feaval';
}


/**
 * Feaval field rule add/edit form
 *
 * @todo
 */
function feaval_settings_field_rule_form($form, $form_state) {
  // set defaults
  $rule_id = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;
  if (!empty($form_state['input'])) {
    $name = !empty($form_state['input']['name']) ? $form_state['input']['name'] : '';
    $entity_type = !empty($form_state['input']['entity_type']) ? $form_state['input']['entity_type'] : '';
    $entity_bundle = !empty($form_state['input']['entity_bundle']) ? $form_state['input']['entity_bundle'] : '';
    $fields_list = !empty($form_state['input']['fields_list']) ? $form_state['input']['fields_list'] : '';
  }
  else {
    if ($rule_id) {
      $rule = feaval_settings_rule_load('field', $rule_id);
      if (empty($rule)) {
        // Return 'page not found' if rule for rule_id doesn't exist.
        drupal_not_found();
        drupal_exit();
      }
      $name = $rule['name'];
      $entity_type = $rule['entity_type'];
      $entity_bundle = $rule['entity_bundle'];
      $fields_list = $rule['fields_list'];
    }
    else {
      $name = '';
      $entity_type = '';
      $entity_bundle = '';
      $fields_list = '';
    }
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $name,
    '#required' => TRUE,
  );
  $form['entity_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity type'),
    '#description' => t('Entity type machine name.'),
    '#default_value' => $entity_type,
    '#required' => TRUE,
    '#element_validate' => array('_feaval_settings_form_textfield_validate'),
  );
  $form['entity_bundle'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity bundle'),
    '#description' => t('Bundle machine name.'),
    '#default_value' => $entity_bundle,
    '#required' => TRUE,
    '#element_validate' => array('_feaval_settings_form_textfield_validate'),
  );
  $form['fields_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Fields list'),
    '#description' => t('List enabled field_names/field_types/widget_types, one for each row. Components must be delimited with "|" character. E.g. "field_name|field_type|widget_type". Any component may be left blank. Use "*" to match all elements, and "-" to exclude certain elements, e.g. "- field_name|field_type|widget_type".'),
    '#default_value' => $fields_list,
    '#required' => TRUE,
    '#element_validate' => array('_feaval_settings_form_textarea_validate'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}


/**
 * Feaval field rule add/edit form validate
 *
 * @todo
 */
function feaval_settings_field_rule_form_validate($form, &$form_state) {
}


/**
 * Feaval field rule add/edit form submit
 *
 * @todo
 */
function feaval_settings_field_rule_form_submit($form, &$form_state) {
  $rule_id = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;

  $rule = array(
    'name' => $form_state['values']['name'],
    'entity_type' => $form_state['values']['entity_type'],
    'entity_bundle' => $form_state['values']['entity_bundle'],
    'fields_list' => $form_state['values']['fields_list'],
  );

  // Save current rule.
  feaval_settings_rule_save('field', $rule, $rule_id);
  drupal_set_message(t('Changes succesfully saved.'));
  $form_state['redirect'] = 'admin/config/system/feaval';
}


/**
 * Page callback for rule deletions.
 */
function feaval_settings_rule_confirm_delete_page($type, $rule_id) {
  if (!in_array($type, array('generic', 'field'))) {
    return MENU_NOT_FOUND;
  }
  if ($rule = feaval_settings_rule_load($type, $rule_id)) {
    return drupal_get_form('feaval_settings_rule_confirm_delete', $type, $rule_id);
  }
  return MENU_NOT_FOUND;
}


/**
 * Form builder; Builds the confirmation form for deleting a single rule.
 */
function feaval_settings_rule_confirm_delete($form, &$form_state, $type, $rule_id) {
  $form['rule_id'] = array(
    '#type' => 'value',
    '#value' => $rule_id,
  );
  $form['rule_type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  return confirm_form($form, t('Are you sure you want to delete the validation rule?'), 'admin/config/system/feaval');
}


/**
 * Process feaval_settings_rule_confirm_delete form submissions.
 */
function feaval_settings_rule_confirm_delete_submit($form, &$form_state) {
  $rule_id = $form['rule_id']['#value'];
  $type = $form['rule_type']['#value'];
  feaval_settings_rule_delete($type, $rule_id);
  drupal_set_message(t('The rule has been deleted.'));
  $form_state['redirect'] = 'admin/config/system/feaval';
}


/**
 * Page callback for rule activation/deactivation.
 */
function feaval_settings_rule_confirm_manage_page($type, $rule_id, $action) {
  if (!in_array($type, array('generic', 'field')) || !in_array($action, array('enable', 'disable'))) {
    return MENU_NOT_FOUND;
  }
  if ($rule = feaval_settings_rule_load($type, $rule_id)) {
    if ($rule['active'] == 1 && $action == 'enable') {
      return t('The rule is already enabled.');
    }
    elseif ($rule['active'] == 0 && $action == 'disable') {
      return t('The rule is already disabled.');
    }
    else {
      return drupal_get_form('feaval_settings_rule_confirm_manage', $type, $rule_id, $action);
    }
  }
  return MENU_NOT_FOUND;
}


/**
 * Form builder; Builds the confirmation form for activation/deactivaion a single rule.
 */
function feaval_settings_rule_confirm_manage($form, &$form_state, $type, $rule_id, $action) {
  $form['rule_id'] = array(
    '#type' => 'value',
    '#value' => $rule_id,
  );
  $form['rule_type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['rule_manage_action'] = array(
    '#type' => 'value',
    '#value' => $action,
  );
  $message = ($action == 'enable')
    ? t('Are you sure you want to enable the validation rule?')
    : t('Are you sure you want to disable the validation rule?');
  return confirm_form($form, $message, 'admin/config/system/feaval', '');
}


/**
 * Process feaval_settings_rule_confirm_manage form submissions.
 */
function feaval_settings_rule_confirm_manage_submit($form, &$form_state) {
  $rule_id = $form['rule_id']['#value'];
  $type = $form['rule_type']['#value'];
  $action = $form['rule_manage_action']['#value'];
  switch ($action) {
  case 'enable' :
    $active = 1;
    $message = t('The rule has been enabled.');
    break;
  case 'disable' :
    $active = 0;
    $message = t('The rule has been disabled.');
    break;
  }
  $query = db_update("feaval_rules_{$type}");
  $query->condition('rule_id', $rule_id);
  $query->fields(array('active' => $active))->execute();
  drupal_set_message($message);
  $form_state['redirect'] = 'admin/config/system/feaval';
}

