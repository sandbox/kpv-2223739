(function ($) {
  // https://www.isovera.com/blog/create-custom-commands-drupal-7-ajax-framework
  // @todo: check
  // Drupal.feaval.fv_toggle = function(ajax, response, status) {

  // Every error has it's unique id, that is doesn't have to be the same
  // as selector.
  Drupal.fv_toggle = function(ajax, response, status) {
    // console.log(response);
    // console.log(response.data);
    $(response.data.selector).checkValidationResult(response.data.error_text);
  };
  Drupal.ajax.prototype.commands.fv_toggle = Drupal.fv_toggle;


  $.fn.checkValidationResult = function (errorText) {

    // see theme_container() and theme_fieldset() form.inc

    console.log(errorText);
    if ($(this).closest(".form-wrapper").length > 0) {
      prevAll = $(this).closest(".form-wrapper").prevAll();
      if (prevAll.length > 0 && prevAll.first().hasClass('feaval-error')) { $(this).closest(".form-wrapper").prevAll().first().remove(); }
      if (errorText != '') { $(this).closest(".form-wrapper").before("<div class='feaval-error messages error'>" + errorText + "</div>"); }
    }
    else {
      prevAll = $(this).closest(".form-item").prevAll();
      if (prevAll.length > 0 && prevAll.first().hasClass('feaval-error')) { $(this).closest(".form-item").prevAll().first().remove(); }
      if (errorText != '') { $(this).closest(".form-item").before("<div class='feaval-error messages error'>" + errorText + "</div>"); }
    }
  };

  Drupal.behaviors.feaval= {
    attach: function (context, settings) {
    }
  };
})(jQuery);

